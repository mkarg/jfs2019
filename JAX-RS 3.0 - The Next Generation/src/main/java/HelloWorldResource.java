import javax.ws.rs.Path;
import javax.ws.rs.GET;

@Path("/question")
public class HelloWorldResource {
  public static final String RESPONSE = "42";

  @GET
  @Path("ultimate")
  public HelloWorldAnswer getResponse() {
    return new HelloWorldAnswer(RESPONSE);
  }
}
