import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HelloWorldAnswer {
  private HelloWorldAnswer() {
    // JAXB
  }

  public String answer;

  public HelloWorldAnswer(String answer) {
    this.answer = answer;
  }
}
